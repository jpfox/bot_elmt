<?php

if(isset($_COOKIE['remindme']) && $_COOKIE['remindme']==1)
	$session_lifetime=3600*24*366;
else
	$session_lifetime=0;
	
if($session_lifetime>0)
	session_set_cookie_params($session_lifetime, '/');

session_start();

if($session_lifetime>0)
	setcookie(session_name(),session_id(),time()+$session_lifetime);
//else
//	setcookie(session_name(),session_id(),0);

if (!ini_get('short_open_tag')) {
	ini_set('short_open_tag', 1);
	if (!ini_get('short_open_tag')) {
		echo "short_open_tag must be set to On in php.ini";
		exit;
	}
}

function __autoload($classname) {
	$filename = BFWK_CLASS_PATH.str_replace('\\','/',$classname).'.class'.BFWK_PHP_EXT;
	include_once($filename);
}

switch(BFWK_MODE)
{
	case 0:	// dev
		error_reporting(E_ALL);
		ini_set('display_errors','stdout');
		break;
	case 1:	// test
		error_reporting(E_ERROR | E_PARSE);
		ini_set('display_errors','stdout');
		break;
	case 2:	// prod
	default:
		error_reporting(0);
		ini_set('display_errors',0);
}

function redirect($ctrl_view, $args=false)
{
	if(strpos($ctrl_view,'://')>0)	// full url
	{
		header('Location: '.$ctrl_view, true, 303);
		exit;
	}
	
	$url = '/?/'.$ctrl_view;
	if($args!==false && count($args)>0)
	{
		$i=0;
		while(isset($args[$i]))
		{
			$url.='/'.urlencode($args[$i]);
			$i++;
		}
		foreach($args as $karg => $varg)
			if(!is_int($karg) || $karg*1>$i)
				$url.='&'.$karg.'='.urlencode($varg);
	}
	header('Location: '.$url, true, 303);
	exit;
}

require_once("Application.class.php");
require_once("Controller.class.php");
require_once("localize.php");

if (isset($_SERVER["QUERY_STRING"]))
	$app = new Application($_SERVER["QUERY_STRING"]);
elseif(isset($argv[1]))
	$app = new Application($argv[1]);
else
	$app = new Application('');

$app->run();
