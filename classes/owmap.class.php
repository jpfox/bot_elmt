<?php

class owmap
{
	
	// Icon codes to UTF-8 Emojis
	public static $iconsWeather = array(
		// day
		'01d' => '🌞', //clear sky
		'02d' => '🌤', //few clouds
		'03d' => '🌥', //scattered clouds
		'04d' => '☁', //broken clouds 
		'09d' => '🌧', //shower rain 
		'10d' => '🌦', //rain
		'11d' => '🌩', //thunderstorm
		'13d' => '❄', //snow
		'50d' => '🌫', //mist
		// night
		'01n' => '🌞', //clear sky
		'02n' => '🌤', //few clouds
		'03n' => '🌥', //scattered clouds
		'04n' => '☁', //broken clouds 
		'09n' => '🌧', //shower rain 
		'10n' => '🌦', //rain
		'11n' => '🌩', //thunderstorm
		'13n' => '❄', //snow
		'50n' => '🌫', //mist
	);

	public static $iconsMoonPhases = array('🌑','🌒','🌓','🌔','🌕','🌖','🌗','🌘');

	
	public static $rWindDir = array('⬇️','↙️','⬅️','↖️','⬆️','↗️','➡️','↘️');
	public static $rWindDirTxt = array('N','NE','E','SE','S','SW','W','NW');
	
	/**
	 * return a forecast object for a city
	 */
	public static function get_forecast($city_id)
	{
		// db connection
		$db = db::getDB();
		$db->insert('SET time_zone = "UTC"');
		
		$dbforecast = $db->getFirst('SELECT * FROM forecasts WHERE id=?',
						array($city_id));
						
		if($dbforecast===false)
		{
			$dbforecast = new dbObj('forecasts');
			$dbforecast->id = $city_id;
		}
		
		if(!isset($dbforecast->updated_at) || $dbforecast->updated_at < date('Y-m-d H:i:s', time()-APP_FORECAST_VALIDITY))
		{
			// Set timeout
			$ctx = stream_context_create(array(
				'http'=>
					array(
						'timeout' => 300,  // 300 seconds
					)
			));
			for($occurs=0; $occurs<4; $occurs++)
			{
				$json = @file_get_contents(APP_OWM_FORECAST_URL."?units=metric&lang=en&id=".$city_id."&APPID=".APP_OWM_API_APPID, false, $ctx);
				if($json===false || strlen($json)<10000)
					sleep(2);
				else
					break;
			}
			if($json!==false && strlen($json)>=10000)
			{
				$dbforecast->forecast_json = $json;
				$dbforecast->update();
			}
			
		}
		
		if(!isset($dbforecast->forecast_json))
			return false;
		
		return json_decode($dbforecast->forecast_json);
	}
	
	/**
	 * convert units 
	 * @param $forecast : forecast object
	 * @param $temp : temperature unit ('K', 'C' or 'F')
	 * @param $speed : wind speed unit ('m/s', 'km/h' or 'miles/hour')
	 * @param $lang : language code ('en', 'fr', 'es'....)
	 * @param $timezone : timezone name ('UTC', 'Europe/Paris'....)
	 */
	public static function forecast_set_units($forecast, $temp, $speed, $lang, $timezone, $city_lat=false, $city_lon=false)
	{
		$DTtzUser = new DateTimeZone($timezone);
		
		$rWeatherCond = get_type_translations('weather', $lang);
		$rDays = get_type_translations('days', $lang);
		$rMoonPhases = get_type_translations('moonphases', $lang);
		
		$forecast->suninfo = array();

		foreach($forecast->list as &$aForecast)
		{
			// date time
			$localdt = new DateTime(date("Y-m-d H:i:s",$aForecast->dt));
			// User TZ
			$localdt->setTimezone($DTtzUser);
			$aForecast->dt_local_DateTime = $localdt;
			$aForecast->dt_local_short = $localdt->format('D H:i');
			$aForecast->dt_local_short_day = $rDays[$localdt->format('N')];
			$aForecast->dt_local_short_time = $localdt->format('H:i');
			$aForecast->dt_local_long = $localdt->format('Y-m-d H:i:s');
			$aForecast->dt_local_long_day = $aForecast->dt_local_short_day . $localdt->format(' d');
			$aForecast->dt_local_date = $localdt->format('Y-m-d');
			$aForecast->dt_local_8601 = $localdt->format('c');
			$aForecast->dt_local_2822 = $localdt->format('r');
			// UTC
			$aForecast->dt_utc_ics_from = gmdate("Ymd\THis\Z",$aForecast->dt);
			$aForecast->dt_utc_ics_to = gmdate("Ymd\THis\Z",$aForecast->dt+300);
			
			$aForecast->dt_timezone = $timezone;
			
			// Sun / Moon 
			if($city_lat!==false || $city_lon!==false)
			{
				// Sun / Moon hourly detail
				/*
				$suncalc = new AurorasLive\SunCalc($localdt, $city_lat, $city_lon);
				
				$moonIllum = $suncalc->getMoonIllumination();
				$angle = $moonIllum['angle'];
				// illuminated fraction of the moon; varies from 0.0 (new moon) to 1.0 (full moon)
				$fraction = $moonIllum['fraction'];
				// moon phase; varies from 0.0 to 1.0 : 0.0 (new moon) 0.25 (1st Q) 0.5 (full moon) 0.75 (last Q)
				$phase = $moonIllum['phase'];
				$phase = round($phase*100)%100;
				if($phase<2)
					$phase_idx=0;	// New
				elseif($phase<24)
					$phase_idx=1;
				elseif($phase<26)
					$phase_idx=2;	// 1st Q
				elseif($phase<49)
					$phase_idx=3;
				elseif($phase<51)
					$phase_idx=4;	// Full
				elseif($phase<74)
					$phase_idx=5;
				elseif($phase<76)
					$phase_idx=6;	// Last Q
				else
					$phase_idx=7;
				
				$aForecast->moondetail = new StdClass();
				$aForecast->moondetail->suncalc = $suncalc;
				$aForecast->moondetail->fraction = $fraction;
				$aForecast->moondetail->phase = $phase;
				$aForecast->moondetail->phase_idx = $phase_idx;
				$aForecast->moondetail->phase_emoji = self::$iconsMoonPhases[$phase_idx];
				$aForecast->moondetail->phase_img = APP_SITE_URL.'/img/moon/phase_'.$phase_idx.'.png';
				$aForecast->moondetail->phase_name = $rMoonPhases[$phase_idx];
				$aForecast->moondetail->angle = $angle;
				*/
				
				$moonPhase = new MoonPhase($localdt->getTimestamp());
				$illumination = $moonPhase->illumination();
				$phase = $moonPhase->phase();
				$phase = round($phase*100)%100;
				if($phase<1)
					$phase_idx=0;	// New
				elseif($phase<24)
					$phase_idx=1;
				elseif($phase<26)
					$phase_idx=2;	// 1st Q
				elseif($phase<49)
					$phase_idx=3;
				elseif($phase<51)
					$phase_idx=4;	// Full
				elseif($phase<74)
					$phase_idx=5;
				elseif($phase<76)
					$phase_idx=6;	// Last Q
				else
					$phase_idx=7;
				$aForecast->moondetail = new StdClass();
				$aForecast->moondetail->illumination = $illumination;
				$aForecast->moondetail->phase = $phase;
				$aForecast->moondetail->phase_idx = $phase_idx;
				$aForecast->moondetail->phase_emoji = self::$iconsMoonPhases[$phase_idx];
				$aForecast->moondetail->phase_img = APP_SITE_URL.'/img/moon/phase_'.$phase_idx.'.png';
				$aForecast->moondetail->phase_name = $rMoonPhases[$phase_idx];
				
				// Sun/moon daily info
				if(!isset($forecast->suninfo[$aForecast->dt_local_date]))
				{
					$localdt12 = new DateTime($aForecast->dt_local_date. " 12:00:00", $DTtzUser);
					$suncalc = new AurorasLive\SunCalc($localdt12, $city_lat, $city_lon);
					
					$sunTimes = $suncalc->getSunTimes();
					$sunrise = $sunTimes['sunrise'];
					//$sunrise->setTimezone($DTtzUser);
					$solarNoon = $sunTimes['solarNoon'];
					//$solarNoon->setTimezone($DTtzUser);
					$sunset = $sunTimes['sunset'];
					//$sunset->setTimezone($DTtzUser);
					
					$moonTimes = $suncalc->getMoonTimes(false);
					if(isset($moonTimes['moonrise']))
					{
						$moonrise = $moonTimes['moonrise'];
						//$moonrise->setTimezone($DTtzUser);
					}
					else
					{
						$moonrise = false;
					}
					if(isset($moonTimes['moonset']))
					{
						$moonset = $moonTimes['moonset'];
						//$moonset->setTimezone($DTtzUser);
					}
					else
					{
						$moonset = false;
					}
					$moonAlwaysUp = isset($moonTimes['alwaysUp'])&&$moonTimes['alwaysUp'];
					$moonAlwaysDown= isset($moonTimes['alwaysDown'])&&$moonTimes['alwaysDown'];
					
					$forecast->suninfo[$aForecast->dt_local_date] = new StdClass();
					$forecast->suninfo[$aForecast->dt_local_date]->suncalc = $suncalc;
					$forecast->suninfo[$aForecast->dt_local_date]->sunrise = $sunrise;
					$forecast->suninfo[$aForecast->dt_local_date]->solarNoon = $solarNoon;
					$forecast->suninfo[$aForecast->dt_local_date]->sunset = $sunset;
					
					$forecast->mooninfo[$aForecast->dt_local_date] = new StdClass();
					$forecast->mooninfo[$aForecast->dt_local_date]->moonrise = $moonrise;
					$forecast->mooninfo[$aForecast->dt_local_date]->moonset = $moonset;
					$forecast->mooninfo[$aForecast->dt_local_date]->alwaysUp = $moonAlwaysUp;
					$forecast->mooninfo[$aForecast->dt_local_date]->alwaysDown = $moonAlwaysDown;
					$moonseq=""; // rise=+HH:SS set=-HH:SS alwaysup=^ alwaysdown=_
					if(  $forecast->mooninfo[$aForecast->dt_local_date]->moonrise!==false
							&& $forecast->mooninfo[$aForecast->dt_local_date]->moonset!==false
							&& $forecast->mooninfo[$aForecast->dt_local_date]->moonrise->format('H:i') <  $forecast->mooninfo[$aForecast->dt_local_date]->moonset->format('H:i'))
					{
						$moonseq .= "+";
						$moonseq .= "+";
						$moonseq .= $forecast->mooninfo[$aForecast->dt_local_date]->moonrise->format('H:i');
						$moonseq .= " ";
						$moonseq .= "-";
						$moonseq .= $forecast->mooninfo[$aForecast->dt_local_date]->moonset->format('H:i');
					}
					 elseif(  $forecast->mooninfo[$aForecast->dt_local_date]->moonrise!==false
							&& $forecast->mooninfo[$aForecast->dt_local_date]->moonset!==false
							&& $forecast->mooninfo[$aForecast->dt_local_date]->moonrise->format('H:i') >  $forecast->mooninfo[$aForecast->dt_local_date]->moonset->format('H:i'))
					{
						$moonseq .= "-";
						$moonseq .= $forecast->mooninfo[$aForecast->dt_local_date]->moonset->format('H:i');
						$moonseq .= " ";
						$moonseq .= "+";
						$moonseq .= $forecast->mooninfo[$aForecast->dt_local_date]->moonrise->format('H:i');
					}
					elseif( $forecast->mooninfo[$aForecast->dt_local_date]->moonrise!==false)
					{
						$moonseq .= "+";
						$moonseq .= $forecast->mooninfo[$aForecast->dt_local_date]->moonrise->format('H:i');
					}
					elseif( $forecast->mooninfo[$aForecast->dt_local_date]->moonset!==false)
					{
						$moonseq .= "-";
						$moonseq .= $forecast->mooninfo[$aForecast->dt_local_date]->moonset->format('H:i');
					}
					elseif( $forecast->mooninfo[$aForecast->dt_local_date]->alwaysUp )
					{ 
						$moonseq .= "^";
					}
					elseif( $forecast->mooninfo[$aForecast->dt_local_date]->alwaysDown )
					{ 
						$moonseq .= "_";
					}
					$forecast->mooninfo[$aForecast->dt_local_date]->moonseq = $moonseq;
					
					// Next MoonPhase
					$localdt00 = new DateTime($aForecast->dt_local_date. " 00:00:00", $DTtzUser);
					$moonPhase = new MoonPhase($localdt00->getTimestamp());
					$moon_phase = $moonPhase->phase();
					if($moon_phase>0.0 && $moon_phase<0.25)
					{
						$next_moon_phase = 2;
						$next_moon_phase_ts = $moonPhase->first_quarter();
					}
					elseif($moon_phase<0.50)
					{
						$next_moon_phase = 4;
						$next_moon_phase_ts = $moonPhase->full_moon();
					}
					elseif($moon_phase<0.75)
					{
						$next_moon_phase = 6;
						$next_moon_phase_ts = $moonPhase->last_quarter();
					}
					else
					{
						$next_moon_phase = 0;
						$next_moon_phase_ts = $moonPhase->next_new_moon();
					}
					$next_phase_dt = new DateTime(date('Y-m-d H:i:s',$next_moon_phase_ts));
					$next_phase_dt->setTimezone($DTtzUser);
					$forecast->mooninfo[$aForecast->dt_local_date]->next_phase_idx = $next_moon_phase;
					$forecast->mooninfo[$aForecast->dt_local_date]->next_phase_emoji = self::$iconsMoonPhases[$next_moon_phase];
					$forecast->mooninfo[$aForecast->dt_local_date]->next_phase_img = APP_SITE_URL.'/img/moon/phase_'.$next_moon_phase.'.png';
					$forecast->mooninfo[$aForecast->dt_local_date]->next_phase_name = $rMoonPhases[$next_moon_phase];
					$forecast->mooninfo[$aForecast->dt_local_date]->next_phase_dt = $next_phase_dt;
					$forecast->mooninfo[$aForecast->dt_local_date]->next_phase_long_day = $rDays[$next_phase_dt->format('N')].$next_phase_dt->format(' d H:i');
				}
			}
			
			// t°
			switch($temp)
			{
				case 'K':
					$aForecast->main->temp = round($aForecast->main->temp + 273.15).' K';
					$aForecast->main->temp_min = round($aForecast->main->temp_min + 273.15).' K';
					$aForecast->main->temp_max = round($aForecast->main->temp_max + 273.15).' K';
				break;
				case 'C':
					$aForecast->main->temp = round($aForecast->main->temp).'°C';
					$aForecast->main->temp_min = round($aForecast->main->temp_min).'°C';
					$aForecast->main->temp_max = round($aForecast->main->temp_max).'°C';
				break;
				case 'F':
					$aForecast->main->temp = round($aForecast->main->temp*1.8+32).'°F';
					$aForecast->main->temp_min = round($aForecast->main->temp_min*1.8+32).'°F';
					$aForecast->main->temp_max = round($aForecast->main->temp_max*1.8+32).'°F';
				break;
			}
			// rain
			if(isset($aForecast->rain->{'3h'}) && round($aForecast->rain->{'3h'})>0)
				$aForecast->rain->volume = round($aForecast->rain->{'3h'}).'mm';
			else
			{
				if(!isset($aForecast->rain))
					$aForecast->rain = new stdClass();
				$aForecast->rain->volume = '';
			}
			// wind speed
			switch($speed)
			{
				case 'm/s':
					$aForecast->wind->speed = $aForecast->wind->speed.' m/s';
				break;
				case 'km/h':
					$aForecast->wind->speed = round($aForecast->wind->speed*3.6).' km/h';
				break;
				case 'miles/hour':
					$aForecast->wind->speed = round($aForecast->wind->speed*2.236936292).' mi/h';
				break;
			}
			$aForecast->wind->short_direction = round($aForecast->wind->deg/45)%8;
			$aForecast->wind->emoji = self::$rWindDir[$aForecast->wind->short_direction];
			$aForecast->wind->dirtxt = self::$rWindDirTxt[$aForecast->wind->short_direction];
			$aForecast->wind->img = APP_SITE_URL.'/img/wind/'.$aForecast->wind->short_direction.'.png';
			
			// weather condition translation
			foreach($aForecast->weather as &$weather)
			{
				$weather->local_description = $rWeatherCond['cond_'.$weather->id];
				$weather->emoji = self::$iconsWeather[$weather->icon];
				$weather->img = APP_SITE_URL.'/img/weather/'.$weather->icon.'.png';
			}
			
		}
		return $forecast;
	}
	
	public static function forecast_format_mastodon($forecast)
	{
		
		return $html;
	}

}
