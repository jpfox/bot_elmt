<?php

/* Test txt file integrity */

header('Content-Type: text/plain; charset=UTF-8', true);

$dir = dirname(__FILE__).'/';

if ($dh = opendir($dir))
{
	while (($file = readdir($dh)) !== false)
	{
		if(substr($file, -4) != '.txt')
			continue;
		echo "\nFile : " . $file;
		if(parse_ini_file($dir.$file)===false)
			echo " error \n";
		else
			echo " OK \n";
	}
	closedir($dh);
}
