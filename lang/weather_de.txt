cond_200 = "Gewitter mit leichtem Regen"
cond_201 = "Gewitter mit Regen"
cond_202 = "Gewitter mit starkem Regen"
cond_210 = "leichtes Gewitter"
cond_211 = "Gewitter"
cond_212 = "starkes Gewitter"
cond_221 = "aufgelockertes Gewitter"
cond_230 = "Gewitter mit leichtem Nieselregen"
cond_231 = "Gewitter mit Nieselregen"
cond_232 = "Gewitter mit starkem Nieselregen"
cond_300 = "leichter intensiver Niesel"
cond_301 = "Nieselregen"
cond_302 = "starker intensiver Nieselregen"
cond_310 = "leichter intensiver Nieselregen"
cond_311 = "Nieselregen"
cond_312 = "starker intensiver Nieselregen"
cond_313 = "Regen und Nieselregen"
cond_314 = "starker Regen und Nieselregen"
cond_321 = "Nieselregen"
cond_500 = "leichter Regen"
cond_501 = "mäßiger Regen"
cond_502 = "Starker Regen"
cond_503 = "sehr starker Regen"
cond_504 = "extremer Regen"
cond_511 = "gefrierender Regen"
cond_520 = "starker intensiver Regenschauer"
cond_521 = "Regenschauer"
cond_522 = "Starker Regenguss"
cond_531 = "aufgelockerter Regenschauer"
cond_600 = "leichter Schnee"
cond_601 = "Schnee"
cond_602 = "starker Schnee"
cond_611 = "Graupel"
cond_612 = "Graupelschauer"
cond_615 = "leichter Regen und Schnee"
cond_616 = "Regen und Schnee"
cond_620 = "leichter Schneeschauer"
cond_621 = "Schneeschauer"
cond_622 = "starker Schneeschauer"
cond_701 = "Nebel"
cond_711 = "Rauch"
cond_721 = "Dunst"
cond_731 = "Sand, Staub wirbelt"
cond_741 = "Nebel"
cond_751 = "Sand"
cond_761 = "Staub"
cond_762 = "vulkanische Asche"
cond_771 = "Böen"
cond_781 = "Tornado"
cond_800 = "klarer Himmel"
cond_801 = "wenige Wolken"
cond_802 = "verstreute Wolken"
cond_803 = "aufgelockerte Wolken"
cond_804 = "bedeckt"
